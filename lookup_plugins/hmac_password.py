from __future__ import absolute_import, division, print_function, unicode_literals

import base64
import hashlib
import hmac

from ansible.plugins.lookup import LookupBase


class LookupModule(LookupBase):
    def run(self, terms, variables=None, **kwargs):
        data = ':'.join(terms)
        key = variables['hmac_password_key'].encode('ascii')
        return [unicode(base64.b64encode(hmac.new(key, data, hashlib.sha256).digest()).strip('='))]
