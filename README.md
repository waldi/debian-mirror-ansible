# Setup for Debian operated mirrors

## Inventory

### Debian project mirrors (`inventory/debian`)

### External mirrors (`inventory/external`)

This file specifies two groups per syncproxy.
The group named `external-$name` allows access to the `debian` repo.
The group named `external-$name+security` allows access to the `debian` and `debian-security` repo.
Other access variants needs a complete mirror spec in `host_vars/$hostname.yaml`.

Each external mirror definition enables both rsync access and ssh push.
The hostname is also used as rsync user name.
Push is configured with a set of extra variables:

`mirror_upstream_push`
: Set to `False` to disable push altogether.

`mirror_upstream_push_host`
: Specify a different hostname for the ssh connection.

`mirror_upstream_push_user`
: Specify the user for the ssh connection.

`mirror_upstream_push_params`
: Specify extra parameters for the ssh connection.

`mirror_upstream_push_delay`
: The absolute amount of time the push should be delayed.
  It is automatically converted into `DELAY` definitions in the runmirrors list.

A static password needs to be defined in the extra password git.

## Playbooks

### site.playbook

Sets up archvsync (ftpsync, runmirrors), rsync secrets and ssh ``authorized_keys`` on all managed mirrors.

### mirrorinfo.playbook

Create a sheet with information for each external mirror including password, hosts to pull from, ssh ``authorized_keys`` and IP of the upstreams.
